# Driver

# python main.py --blksize 2 --chuse 1 --objfn AWGN --n0 0.2 --sigma2 3.00 --epochs 2500 --models 100 --prefix ./models/02x01/cau_awgn_64_32_16_n020_sigma0300
# python main_oshea.py --blksize 2 --chuse 1 --snr 10.0 --epochs 2500 --models 100 --prefix ./models/02x01/cau_oshea_64_32_16_10dB
# python main.py --blksize 4 --chuse 2 --objfn AWGN --n0 0.4 --sigma2 5.00 --epochs 2500 --models 100 --prefix ./models/04x02/cau_awgn_64_32_16_n020_sigma0500
# python main.py --blksize 8 --chuse 4 --objfn AWGN --n0 0.8 --sigma2 5.00 --epochs 2500 --models 100 --prefix ./models/08x04/cau_awgn_64_32_16_n020_sigma0500

# python main.py --blksize 2 --chuse 1 --objfn RBF --n0 0.2 --sigma2 3.00 --epochs 2500 --models 100 --prefix ./models/02x01/cau_rbf_64_32_16_n020_sigma0300
# python main.py --blksize 4 --chuse 2 --objfn RBF --n0 0.4 --sigma2 5.00 --epochs 2500 --models 100 --prefix ./models/04x02/cau_rbf_64_32_16_n020_sigma0500
# python main_oshea.py --blksize 4 --chuse 2 --snr 10.0 --epochs 2500 --models 100 --prefix ./models/04x02/cau_oshea_64_32_16_10dB
# python main.py --blksize 8 --chuse 4 --objfn RBF --n0 0.8 --sigma2 5.00 --epochs 2500 --models 100 --prefix ./models/08x04/cau_rbf_64_32_16_n020_sigma0500

# python main.py --blksize 2 --chuse 1 --objfn CAU --n0 0.2 --sigma2 3.00 --epochs 2500 --models 100 --prefix ./models/02x01/cau_cau_64_32_16_n020_sigma0300
# python main.py --blksize 4 --chuse 2 --objfn CAU --n0 0.4 --sigma2 5.00 --epochs 2500 --models 100 --prefix ./models/04x02/cau_cau_64_32_16_n020_sigma0500
# python main.py --blksize 8 --chuse 4 --objfn CAU --n0 0.8 --sigma2 5.00 --epochs 2500 --models 100 --prefix ./models/08x04/cau_cau_64_32_16_n020_sigma0500
# python main_oshea.py --blksize 8 --chuse 4 --snr 10.0 --epochs 2500 --models 100 --prefix ./models/08x04/cau_oshea_64_32_16_10dB

# TCCN - test
# python main.py --blksize 2 --chuse 1 --objfn CAU --n0 0.2 --sigma2 3.00 --epochs 2000 --models 25 --prefix ./models/02x01/cau_cau_64_32_16_n020_sigma0300
# python main_oshea.py --blksize 2 --chuse 1 --snr 10.0 --epochs 2000 --models 25 --prefix ./models/02x01/cau_oshea_64_32_16_10dB

# TCCN - r1
# python main.py --blksize 2 --chuse 1 --objfn CAU --n0 0.2 --sigma2 3.00 --epochs 2000 --models 100 --prefix ./models/02x01/cau_cau_64_32_16_n020_sigma0300
# python main_oshea.py --blksize 2 --chuse 1 --snr 5.0 --epochs 2000 --models 100 --prefix ./models/02x01/cau_oshea_64_32_16_05dB
# python main.py --blksize 2 --chuse 1 --objfn AWGN --n0 0.2 --sigma2 3.00 --epochs 2000 --models 100 --prefix ./models/02x01/cau_awgn_64_32_16_n020_sigma0300
# python main.py --blksize 2 --chuse 1 --objfn RBF --n0 0.2 --sigma2 3.00 --epochs 2000 --models 100 --prefix ./models/02x01/cau_rbf_64_32_16_n020_sigma0300

# python main.py --blksize 4 --chuse 2 --objfn CAU --n0 0.4 --sigma2 5.00 --epochs 2000 --models 100 --prefix ./models/04x02/cau_cau_64_32_16_n020_sigma0500
# python main_oshea.py --blksize 4 --chuse 2 --snr 20.0 --epochs 2000 --models 100 --prefix ./models/04x02/cau_oshea_64_32_16_20dB
# python main.py --blksize 4 --chuse 2 --objfn AWGN --n0 0.4 --sigma2 5.00 --epochs 2000 --models 100 --prefix ./models/04x02/cau_awgn_64_32_16_n020_sigma0500
# python main.py --blksize 4 --chuse 2 --objfn RBF --n0 0.4 --sigma2 5.00 --epochs 2000 --models 100 --prefix ./models/04x02/cau_rbf_64_32_16_n020_sigma0500

# python main.py --blksize 8 --chuse 4 --objfn CAU --n0 0.8 --sigma2 3.00 --epochs 2000 --models 100 --prefix ./models/08x04/cau_cau_64_32_16_n020_sigma0300
python main_oshea.py --blksize 8 --chuse 4 --snr 10.0 --epochs 2000 --models 100 --prefix ./models/08x04/cau_oshea_64_32_16_10dB
# python main.py --blksize 8 --chuse 4 --objfn AWGN --n0 0.8 --sigma2 5.00 --epochs 2000 --models 100 --prefix ./models/08x04/cau_awgn_64_32_16_n020_sigma0500
# python main.py --blksize 8 --chuse 4 --objfn RBF --n0 0.8 --sigma2 5.00 --epochs 2000 --models 100 --prefix ./models/08x04/cau_rbf_64_32_16_n020_sigma0500
