# Driver

# python main.py --blksize 8 --chuse 4 --objfn AWGN --n0 0.8 --epochs 2500 --models 100 --prefix ./models/awgn_awgn_64_32_16_n080
# python main.py --blksize 8 --chuse 4 --objfn AWGN --n0 0.4 --epochs 2500 --models 100 --prefix ./models/awgn_awgn_64_32_16_n040
# python main.py --blksize 8 --chuse 4 --objfn AWGN --n0 1.2 --epochs 2500 --models 100 --prefix ./models/awgn_awgn_64_32_16_n120
# python main_oshea.py --blksize 8 --chuse 4 --snr 0.0 --epochs 2500 --models 100 --prefix ./models/awgn_oshea_64_32_16_00dB

# python main.py --blksize 8 --chuse 4 --objfn AWGN --n0 0.8 --sigma2 0.10 --epochs 2500 --models 100 --prefix ./models/08x04/sigma2_010/awgn_awgn_64_32_16_n080
# python main.py --blksize 8 --chuse 4 --objfn AWGN --n0 0.8 --sigma2 0.50 --epochs 2500 --models 100 --prefix ./models/08x04/sigma2_050/awgn_awgn_64_32_16_n080
python main.py --blksize 8 --chuse 4 --objfn AWGN --n0 0.8 --sigma2 1.00 --epochs 2500 --models 100 --sigma2_int 500 --prefix ./models/08x04/sigma2_100/awgn_awgn_64_32_16_n080_u500
# python main.py --blksize 8 --chuse 4 --objfn AWGN --n0 0.8 --sigma2 1.50 --epochs 2500 --models 100 --prefix ./models/08x04/sigma2_150/awgn_awgn_64_32_16_n080
# python main.py --blksize 8 --chuse 4 --objfn AWGN --n0 0.8 --sigma2 2.00 --epochs 2500 --models 100 --prefix ./models/08x04/sigma2_200/awgn_awgn_64_32_16_n080
# python main.py --blksize 8 --chuse 4 --objfn AWGN --n0 0.8 --sigma2 2.50 --epochs 2500 --models 100 --prefix ./models/08x04/sigma2_250/awgn_awgn_64_32_16_n080

# python main.py --blksize 4 --chuse 2 --objfn AWGN --n0 0.4 --sigma2 0.10 --epochs 2500 --models 100 --prefix ./models/04x02/sigma2_010/awgn_awgn_64_32_16_n040
# python main.py --blksize 4 --chuse 2 --objfn AWGN --n0 0.4 --sigma2 0.50 --epochs 2500 --models 100 --prefix ./models/04x02/sigma2_050/awgn_awgn_64_32_16_n040
# python main.py --blksize 4 --chuse 2 --objfn AWGN --n0 0.4 --sigma2 1.00 --epochs 2500 --models 100 --prefix ./models/04x02/sigma2_100/awgn_awgn_64_32_16_n040
# python main.py --blksize 4 --chuse 2 --objfn AWGN --n0 0.4 --sigma2 1.50 --epochs 2500 --models 100 --prefix ./models/04x02/sigma2_150/awgn_awgn_64_32_16_n040
# python main.py --blksize 4 --chuse 2 --objfn AWGN --n0 0.4 --sigma2 2.00 --epochs 2500 --models 100 --prefix ./models/04x02/sigma2_200/awgn_awgn_64_32_16_n040
# python main.py --blksize 4 --chuse 2 --objfn AWGN --n0 0.4 --sigma2 2.50 --epochs 2500 --models 100 --prefix ./models/04x02/sigma2_250/awgn_awgn_64_32_16_n040

# python main.py --blksize 2 --chuse 1 --objfn AWGN --n0 0.2 --sigma2 0.10 --epochs 2500 --models 100 --prefix ./models/02x01/sigma2_010/awgn_awgn_64_32_16_n020
# python main.py --blksize 2 --chuse 1 --objfn AWGN --n0 0.2 --sigma2 0.50 --epochs 2500 --models 100 --prefix ./models/02x01/sigma2_050/awgn_awgn_64_32_16_n020
# python main.py --blksize 2 --chuse 1 --objfn AWGN --n0 0.2 --sigma2 1.00 --epochs 2500 --models 100 --prefix ./models/02x01/sigma2_100/awgn_awgn_64_32_16_n020
# python main.py --blksize 2 --chuse 1 --objfn AWGN --n0 0.2 --sigma2 1.50 --epochs 2500 --models 100 --prefix ./models/02x01/sigma2_150/awgn_awgn_64_32_16_n020
# python main.py --blksize 2 --chuse 1 --objfn AWGN --n0 0.2 --sigma2 2.00 --epochs 2500 --models 100 --prefix ./models/02x01/sigma2_200/awgn_awgn_64_32_16_n020
# python main.py --blksize 2 --chuse 1 --objfn AWGN --n0 0.2 --sigma2 2.50 --epochs 2500 --models 100 --prefix ./models/02x01/sigma2_250/awgn_awgn_64_32_16_n020

python main.py --blksize 4 --chuse 1 --objfn AWGN --n0 0.2 --sigma2 1.00 --epochs 2500 --models 100 --sigma2_int 500 --prefix ./models/04x01/sigma2_100/awgn_awgn_64_32_16_n020_u500

python main.py --blksize 4 --chuse 1 --objfn RBF --n0 0.2 --sigma2 1.00 --epochs 2500 --models 100 --sigma2_int 500 --prefix ./models/04x01/sigma2_100/awgn_rbf_64_32_16_n020_u500