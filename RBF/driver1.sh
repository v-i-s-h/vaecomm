# Driver

# TCCN Review
# time python main.py --blksize 2 --chuse 1 --objfn RBF --n0 0.2 --epochs 3000 --models 100 --prefix ./models/02x01/rbf_rbf_64_32_16_n020
# time python main.py --blksize 2 --chuse 1 --objfn AWGN --n0 0.2 --epochs 3000 --models 100 --prefix ./models/02x01/rbf_awgn_64_32_16_n020
# time python main_oshea.py --blksize 2 --chuse 1 --snr 10.0 --epochs 3000 --models 100 --prefix ./models/02x01/rbf_oshea_64_32_16_10dB

# time python main.py --blksize 4 --chuse 2 --objfn RBF --n0 0.4 --epochs 3000 --models 100 --prefix ./models/04x02/rbf_rbf_64_32_16_n040
# time python main.py --blksize 4 --chuse 2 --objfn AWGN --n0 0.4 --epochs 3000 --models 100 --prefix ./models/04x02/rbf_awgn_64_32_16_n040
# time python main_oshea.py --blksize 4 --chuse 2 --snr 10.0 --epochs 3000 --models 100 --prefix ./models/04x02/rbf_oshea_64_32_16_10dB

time python main.py --blksize 8 --chuse 4 --objfn RBF --n0 0.8 --epochs 3000 --models 100 --prefix ./models/08x04/rbf_rbf_64_32_16_n080
time python main.py --blksize 8 --chuse 4 --objfn AWGN --n0 0.8 --epochs 3000 --models 100 --prefix ./models/08x04/rbf_awgn_64_32_16_n080
time python main_oshea.py --blksize 8 --chuse 4 --snr 10.0 --epochs 3000 --models 100 --prefix ./models/08x04/rbf_oshea_64_32_16_10dB