### How to use?

1. Run `main.py` with appropriate options. Refer `driver.sh` for examples.
2. Run `analysis_energy.ipynb` and get `*_summary.h5` files from `*_summary.dil`. This will create summary files with packing density information.
3. Run `analysis_figures.ipynb` to plot the figures. Input should be `*_summary.h5`.